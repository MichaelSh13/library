import * as fs from 'fs';
import { execSync } from 'child_process';

const migrationsPath = 'src/modules/database/migrations';
const defaultMigrationName = 'init';

const command = process.argv[2];
const migrationName = process.argv[3];

// Check if the migrations folder is empty
const isMigrationsFolderEmpty =
  !fs.existsSync(migrationsPath) || fs.readdirSync(migrationsPath).length === 0;

// Determine the migration name based on the provided command argument or the default name
const resolvedMigrationName =
  migrationName || (isMigrationsFolderEmpty ? defaultMigrationName : null);

if (!resolvedMigrationName && command !== 'revert' && command !== 'run') {
  console.error('Error: Migration name must be provided.');
  process.exit(1);
}

// Run the TypeORM migration command based on the specified command and migration name
switch (command) {
  case 'generate':
    execSync(
      `pnpm run typeorm migration:generate ${migrationsPath}/${resolvedMigrationName}`,
    );
    break;
  case 'create':
    execSync(
      `pnpm run typeorm migration:create ${migrationsPath}/${resolvedMigrationName}`,
    );
    break;
  case 'run':
    execSync(`pnpm run typeorm migration:run`);
    break;
  case 'revert':
    execSync(`pnpm run typeorm migration:revert`);
    break;
  default:
    console.error(`Error: Unknown command '${command}'.`);
    process.exit(1);
}
