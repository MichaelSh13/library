import * as mysql2 from 'mysql2/promise';
import { DataSource, DataSourceOptions } from 'typeorm';

import * as dotenv from 'dotenv';

dotenv.config();

// TODO?: Config for DB is here because it just easier to understand and work with migrations.

export const dataSourceOptions: DataSourceOptions = {
  type: 'mysql',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  synchronize: false,
  migrationsRun: true,
  // logging: false,
  entities: ['dist/**/*.entity.js'],
  subscribers: ['dist/**/*.subscriber.js'],
  // migrationsTableName: "custom_migration_table",
  migrations: ['dist/src/modules/database/migrations/*.js'],
  connectTimeout: 60 * 1000,
  driver: mysql2.createConnection,
};

const dataSource = new DataSource(dataSourceOptions);

export default dataSource;
