import {
  ClassSerializerInterceptor,
  Module,
  ValidationPipe,
} from '@nestjs/common';
import { GraphqlModule } from './modules/graphql/graphql.module';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { ValidationConfig } from './modules/configuration/configs/validation.config';
import { ConfigurationModule } from './modules/configuration/configuration.module';
import { DatabaseModule } from './modules/database/database.module';
import { CoreModule } from './modules/core/core.module';
import { AppResolver } from './app.resolver';

// TODO: Configure eslint better.

@Module({
  imports: [
    ConfigurationModule.forRoot(),
    DatabaseModule.forRoot(),
    GraphqlModule,

    CoreModule,
  ],
  providers: [
    AppResolver,
    {
      provide: APP_PIPE,
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const { pipeOptions, validationOptions } =
          configService.getOrThrow<ValidationConfig>('validation');

        return new ValidationPipe({ ...pipeOptions, ...validationOptions });
      },
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
  ],
})
export class AppModule {}
