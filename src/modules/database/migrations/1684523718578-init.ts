import { MigrationInterface, QueryRunner } from 'typeorm';

export class Init1684523718578 implements MigrationInterface {
  name = 'Init1684523718578';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`book_entity\` (\`id\` varchar(36) NOT NULL, \`createdDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedDate\` datetime(6) NULL, \`title\` varchar(200) NOT NULL, \`description\` varchar(2000) NOT NULL, UNIQUE INDEX \`IDX_2c390f563075ef2501a7ada8e3\` (\`title\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`author_entity\` (\`id\` varchar(36) NOT NULL, \`createdDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedDate\` datetime(6) NULL, \`name\` varchar(200) NOT NULL, \`nickname\` varchar(200) NOT NULL, UNIQUE INDEX \`IDX_bba2f717ab5e01e0f64b3dce0b\` (\`nickname\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`book_entity_authors_author_entity\` (\`bookEntityId\` varchar(255) NOT NULL, \`authorEntityId\` varchar(255) NOT NULL, INDEX \`IDX_3bb30e7c32191521202457dcae\` (\`bookEntityId\`), INDEX \`IDX_55d59c177f6423a819bb9d7253\` (\`authorEntityId\`), PRIMARY KEY (\`bookEntityId\`, \`authorEntityId\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`book_entity_authors_author_entity\` ADD CONSTRAINT \`FK_3bb30e7c32191521202457dcae1\` FOREIGN KEY (\`bookEntityId\`) REFERENCES \`book_entity\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE \`book_entity_authors_author_entity\` ADD CONSTRAINT \`FK_55d59c177f6423a819bb9d72535\` FOREIGN KEY (\`authorEntityId\`) REFERENCES \`author_entity\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`book_entity_authors_author_entity\` DROP FOREIGN KEY \`FK_55d59c177f6423a819bb9d72535\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`book_entity_authors_author_entity\` DROP FOREIGN KEY \`FK_3bb30e7c32191521202457dcae1\``,
    );
    await queryRunner.query(
      `DROP INDEX \`IDX_55d59c177f6423a819bb9d7253\` ON \`book_entity_authors_author_entity\``,
    );
    await queryRunner.query(
      `DROP INDEX \`IDX_3bb30e7c32191521202457dcae\` ON \`book_entity_authors_author_entity\``,
    );
    await queryRunner.query(`DROP TABLE \`book_entity_authors_author_entity\``);
    await queryRunner.query(
      `DROP INDEX \`IDX_bba2f717ab5e01e0f64b3dce0b\` ON \`author_entity\``,
    );
    await queryRunner.query(`DROP TABLE \`author_entity\``);
    await queryRunner.query(
      `DROP INDEX \`IDX_2c390f563075ef2501a7ada8e3\` ON \`book_entity\``,
    );
    await queryRunner.query(`DROP TABLE \`book_entity\``);
  }
}
