import type { DynamicModule } from '@nestjs/common';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import type { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({})
export class DatabaseModule {
  static forRoot(options: TypeOrmModuleOptions = {}): DynamicModule {
    return {
      module: DatabaseModule,
      imports: [
        TypeOrmModule.forRootAsync({
          inject: [ConfigService],
          useFactory: async (configService: ConfigService) =>
            // TODO: Use ConfigServices enum instead of 'database'. Change in all places!
            ({
              ...configService.getOrThrow<TypeOrmModuleOptions>('database'),
              ...options,
            } as TypeOrmModuleOptions),
        }),
      ],
    };
  }
}
