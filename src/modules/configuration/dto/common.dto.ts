import { IsNumber } from 'class-validator';

export class CommonDto {
  @IsNumber()
  port: number;
}
