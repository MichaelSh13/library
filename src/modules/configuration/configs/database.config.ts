import { registerAs } from '@nestjs/config';
import type { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';

import { DatabaseDto } from '../dto/database.dto';
import { dataSourceOptions } from 'db/data-source';

export default registerAs(
  'database',
  async (): Promise<TypeOrmModuleOptions> => {
    const options: TypeOrmModuleOptions = {
      ...dataSourceOptions,
      autoLoadEntities: true,
    };

    const instance = plainToInstance(DatabaseDto, options);
    const errors = await validate(instance);
    errors.forEach((error) => {
      throw new Error(error.toString());
    });

    return instance;
  },
);
