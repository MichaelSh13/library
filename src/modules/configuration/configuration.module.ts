import type { DynamicModule } from '@nestjs/common';
import { Module } from '@nestjs/common';
import type { ConfigModuleOptions } from '@nestjs/config';
import { ConfigModule } from '@nestjs/config';

import common from './configs/common.config';
import database from './configs/database.config';
import validations from './configs/validation.config';

@Module({})
export class ConfigurationModule {
  static forRoot(options: ConfigModuleOptions = {}): DynamicModule {
    return {
      module: ConfigurationModule,
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          load: [common, database, validations],
          ...options,
        }),
      ],
      exports: [ConfigModule],
    };
  }
}
