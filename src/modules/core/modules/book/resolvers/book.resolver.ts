import {
  Args,
  ID,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { BookService } from '../services/book.service';
import { BookEntity } from '../entities/book.entity';
import { ParseUUIDPipe } from '@nestjs/common';
import { CreateBookInputDto } from '../dto/create-book.input.dto';
import { AuthorEntity } from '../../author/entities/author.entity';

@Resolver(() => BookEntity)
export class BookResolver {
  constructor(private readonly bookService: BookService) {}

  @Mutation(() => BookEntity)
  createBook(
    @Args('bookData') createBookData: CreateBookInputDto,
    @Args('authorId', { type: () => ID }, new ParseUUIDPipe()) authorId: string,
  ) {
    return this.bookService.createBook(authorId, createBookData);
  }

  @Mutation(() => BookEntity)
  assignAuthorToBook(
    @Args('existAuthorId', { type: () => ID }, new ParseUUIDPipe())
    existAuthorId: string,
    @Args('assignToAuthorId', { type: () => ID }, new ParseUUIDPipe())
    assignToAuthorId: string,
    @Args('bookId', { type: () => ID }, new ParseUUIDPipe()) bookId: string,
  ) {
    return this.bookService.assignToBook(
      bookId,
      existAuthorId,
      assignToAuthorId,
    );
  }

  @Mutation(() => BookEntity)
  removeSelf(
    @Args('authorId', { type: () => ID }, new ParseUUIDPipe()) authorId: string,
    @Args('bookId', { type: () => ID }, new ParseUUIDPipe()) bookId: string,
  ) {
    return this.bookService.removeAuthorFromBook(authorId, bookId);
  }

  @Query(() => BookEntity)
  async book(@Args('id', { type: () => ID }, new ParseUUIDPipe()) id: string) {
    return this.bookService.getBook({ where: { id } });
  }

  @Query(() => [BookEntity])
  books() {
    return this.bookService.getAllBooks();
  }

  @ResolveField(() => [AuthorEntity])
  async authors(@Parent() { id }: BookEntity) {
    // TODO: Find better way to get relations.
    const { authors } = await this.bookService.getBook({
      where: { id },
      relations: ['authors'],
    });

    return authors;
  }
}
