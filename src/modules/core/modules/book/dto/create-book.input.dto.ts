import { Field, InputType } from '@nestjs/graphql';
import { IsString, MaxLength, MinLength } from 'class-validator';

@InputType()
export class CreateBookInputDto {
  @Field(() => String)
  @IsString()
  @MinLength(2)
  @MaxLength(200)
  title: string;

  @Field(() => String)
  @IsString()
  @MinLength(2)
  @MaxLength(2000)
  description: string;
}
