import {
  BadRequestException,
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BookEntity } from '../entities/book.entity';
import { FindOneOptions, Repository } from 'typeorm';
import { CreateBookInputDto } from '../dto/create-book.input.dto';
import { AuthorService } from '../../author/services/author.service';

@Injectable()
export class BookService {
  constructor(
    @InjectRepository(BookEntity)
    private readonly bookRepository: Repository<BookEntity>,

    private readonly authorService: AuthorService,
  ) {}

  async createBook(
    authorId: string,
    { description, title }: CreateBookInputDto,
  ) {
    const author = await this.authorService.getAuthor({
      where: { id: authorId },
    });

    const bookExist = await this.bookRepository.findOneBy({ title });
    if (bookExist) {
      throw new ConflictException('Book with such title already exist.');
    }

    const bookInst = this.bookRepository.create({
      title,
      description,
      authors: [author],
    });
    try {
      const book = await this.bookRepository.save(bookInst);

      return book;
    } catch (err) {
      // TODO: Add logger;
      console.log('createBook, save, error: ', err);

      throw new BadRequestException('Book not created.');
    }
  }

  async assignToBook(
    bookId: string,
    existAuthorId: string,
    assignToAuthorId: string,
  ) {
    const book = await this.bookRepository.findOne({
      where: { id: bookId },
      relations: ['authors'],
    });
    if (!book) {
      throw new NotFoundException('Book not found.');
    }
    const authorsId = book.authors.map(({ id }) => id);
    if (!authorsId.includes(existAuthorId)) {
      throw new ForbiddenException('You are not Author of this book.');
    }
    if (authorsId.includes(assignToAuthorId)) {
      throw new ConflictException('Such Author already assigned.');
    }

    const authorToAssign = await this.authorService.getAuthor({
      where: { id: assignToAuthorId },
    });

    const updatedBookInst = this.bookRepository.create({
      ...book,
      authors: [...book.authors, authorToAssign],
    });
    try {
      const updatedBook = this.bookRepository.save(updatedBookInst);

      return updatedBook;
    } catch (err) {
      // TODO: Add logger;
      console.log('assignToBook, save, error: ', err);

      throw new BadRequestException('Author not assigned.');
    }
  }

  async removeAuthorFromBook(authorId: string, bookId: string) {
    const book = await this.bookRepository.findOne({
      where: { id: bookId },
      relations: ['authors'],
    });
    if (!book) {
      throw new NotFoundException('Book not found.');
    }

    const authorIndex = book.authors.findIndex(({ id }) => id === authorId);
    if (authorIndex === -1) {
      throw new ForbiddenException(`You are not in Author of this Book.`);
    }

    if (book.authors.length <= 1) {
      throw new BadRequestException('Book must have at least 1 Author.');
    }

    book.authors.splice(authorIndex, 1);
    try {
      const updatedBook = this.bookRepository.save(book);

      return updatedBook;
    } catch (err) {
      // TODO: Add logger;
      console.log('removeAuthorFromBook, save, error: ', err);

      throw new BadRequestException('Author not removed.');
    }
  }

  async getBook(options: FindOneOptions<BookEntity>) {
    const book = await this.bookRepository.findOne(options);
    if (!book) {
      throw new NotFoundException('Book not found.');
    }

    return book;
  }

  getAllBooks() {
    return this.bookRepository.find();
  }
}
