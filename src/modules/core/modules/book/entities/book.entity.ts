import { Field, ObjectType } from '@nestjs/graphql';
import { CustomBaseEntity } from 'src/entities/custom-base.entity';
import { Column, Entity, JoinTable, ManyToMany } from 'typeorm';
import { AuthorEntity } from '../../author/entities/author.entity';

@ObjectType()
@Entity()
export class BookEntity extends CustomBaseEntity {
  @Field(() => String)
  @Column('varchar', {
    length: 200,
    unique: true,
  })
  title: string;

  @Field(() => String)
  @Column('varchar', {
    length: 2000,
  })
  description: string;

  @Field(() => [AuthorEntity])
  @ManyToMany(() => AuthorEntity, ({ books }) => books)
  @JoinTable()
  authors?: AuthorEntity[];
}
