import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookEntity } from './entities/book.entity';
import { BookService } from './services/book.service';
import { BookResolver } from './resolvers/book.resolver';
import { AuthorModule } from '../author/author.module';

@Module({
  imports: [TypeOrmModule.forFeature([BookEntity]), AuthorModule],
  providers: [BookService, BookResolver],
  exports: [BookService],
})
export class BookModule {}
