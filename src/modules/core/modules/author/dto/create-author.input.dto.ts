import { Field, InputType } from '@nestjs/graphql';
import { IsString, MaxLength, MinLength } from 'class-validator';

@InputType()
export class CreateAuthorInputDto {
  @Field(() => String)
  @IsString()
  @MinLength(2)
  @MaxLength(200)
  name: string;

  @Field(() => String)
  @IsString()
  @MinLength(2)
  @MaxLength(200)
  nickname: string;
}
