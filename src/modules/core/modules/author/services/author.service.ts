import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateAuthorInputDto } from '../dto/create-author.input.dto';
import { FindOneOptions, Repository } from 'typeorm';
import { AuthorEntity } from '../entities/author.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AuthorService {
  constructor(
    @InjectRepository(AuthorEntity)
    private readonly authorRepository: Repository<AuthorEntity>,
  ) {}

  async createAuthor({ name, nickname }: CreateAuthorInputDto) {
    const authorExist = await this.authorRepository.findOneBy({
      nickname,
    });
    if (authorExist) {
      throw new ConflictException('Author with such nickname already exist.');
    }

    const authorInst = this.authorRepository.create({ name, nickname });
    try {
      const author = await this.authorRepository.save(authorInst);

      return author;
    } catch (err) {
      // TODO: Add logger;
      console.log('createAuthor, save, error: ', err);

      throw new BadRequestException('Author not created.');
    }
  }

  async getAuthor(options: FindOneOptions<AuthorEntity>) {
    const author = await this.authorRepository.findOne(options);
    if (!author) {
      throw new NotFoundException('Author not found.');
    }

    return author;
  }

  getAllAuthors() {
    return this.authorRepository.find();
  }
}
