import { Field, ObjectType } from '@nestjs/graphql';
import { CustomBaseEntity } from 'src/entities/custom-base.entity';
import { Column, Entity, ManyToMany } from 'typeorm';
import { BookEntity } from '../../book/entities/book.entity';

@ObjectType()
@Entity()
export class AuthorEntity extends CustomBaseEntity {
  @Field(() => String)
  @Column('varchar', {
    length: 200,
  })
  name: string;

  @Field(() => String)
  @Column('varchar', {
    length: 200,
    unique: true,
  })
  nickname: string;

  @Field(() => [BookEntity])
  @ManyToMany(() => BookEntity, ({ authors }) => authors)
  books?: BookEntity[];
}
