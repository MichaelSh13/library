import {
  Args,
  ID,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { AuthorService } from '../services/author.service';
import { AuthorEntity } from '../entities/author.entity';
import { CreateAuthorInputDto } from '../dto/create-author.input.dto';
import { ParseUUIDPipe } from '@nestjs/common';
import { BookService } from '../../book/services/book.service';
import { BookEntity } from '../../book/entities/book.entity';

@Resolver(() => AuthorEntity)
export class AuthorResolver {
  constructor(private readonly authorService: AuthorService) {}

  @Mutation(() => AuthorEntity)
  createAuthor(@Args('authorData') createAuthorData: CreateAuthorInputDto) {
    return this.authorService.createAuthor(createAuthorData);
  }

  @Query(() => AuthorEntity)
  async author(
    @Args('id', { type: () => ID }, new ParseUUIDPipe()) id: string,
  ) {
    return this.authorService.getAuthor({ where: { id } });
  }

  @Query(() => [AuthorEntity])
  authors() {
    return this.authorService.getAllAuthors();
  }

  @ResolveField(() => [BookEntity])
  async books(@Parent() { id }: AuthorEntity) {
    // TODO: Find better way to get relations.
    const { books } = await this.authorService.getAuthor({
      where: { id },
      relations: ['books'],
    });

    return books;
  }
}
