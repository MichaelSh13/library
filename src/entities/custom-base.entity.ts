import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Exclude } from 'class-transformer';
import {
  CreateDateColumn,
  DeleteDateColumn,
  Generated,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@ObjectType({ isAbstract: true })
export abstract class CustomBaseEntity {
  @Field(() => ID)
  // @PrimaryGeneratedColumn('uuid')
  @PrimaryColumn()
  @Generated('uuid')
  id!: string;

  @Field(() => Date)
  @CreateDateColumn()
  createdDate!: Date;

  @Field(() => Date)
  @UpdateDateColumn()
  updatedDate!: Date;

  @Field(() => Date, { nullable: true })
  @Exclude()
  @DeleteDateColumn()
  deletedDate?: Date;
}
